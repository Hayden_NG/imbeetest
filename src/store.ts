import { combineReducers, createStore } from "redux";
import { tagReducer } from "trendingTag/reducer";
import { Tag } from "trendingTag/reducer";

export interface RootState {
  tagReducer: Tag;
}

const reducer = combineReducers<RootState>({ tagReducer: tagReducer });

export const store = createStore(reducer);
