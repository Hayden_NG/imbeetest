import React, { useState } from "react";
import { Searchbar } from "react-native-paper";
import { StyleSheet } from "react-native";

export const Searching = () => {
  const [searchQuery, setSearchQuery] = useState("");

  const onChangeSearch = (query: string) => {
    setSearchQuery(query);
  };

  return (
    <Searchbar value={searchQuery} onChangeText={onChangeSearch} style={styles.SearchbarStyle} />
  );
};

const styles = StyleSheet.create({
  SearchbarStyle: {
    margin: 8,
  },
});
