import React, { useEffect, useState } from "react";
import { Dimensions, FlatList, Pressable, StyleSheet, Text } from "react-native";
import { useDispatch } from "react-redux";
import { loadTag } from "trendingTag/actions";

export const TabRow = () => {
  const [hotTags, setHotTags] = useState<any>();
  const [selectedTag, setSelectedTag] = useState();
  const dispatch = useDispatch();

  const getData = async () => {
    try {
      const res = await fetch(
        "https://api.stackexchange.com/2.2/tags?pagesize=10&order=desc&sort=popular&site=stackoverflow&filter=!-.G.68h(ttpy"
      );
      const data = await res.json();
      setHotTags(data.items);
      setSelectedTag(data.items[0].name);
      dispatch(loadTag({ tag: data.items[0].name }));
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      horizontal
      style={styles.container}
      contentContainerStyle={{ justifyContent: "center", alignItems: "center" }}
      data={hotTags}
      renderItem={({ item }) => (
        <Pressable
          style={
            selectedTag === item.name
              ? styles.selectedTab
              : { ...styles.selectedTab, backgroundColor: "white" }
          }
          onPress={() => {
            setSelectedTag(item.name);
            dispatch(loadTag({ tag: item.name }));
          }}>
          <Text>{item.name}</Text>
        </Pressable>
      )}
      keyExtractor={(item) => item.name}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height / 12,
  },
  selectedTab: {
    height: 40,
    backgroundColor: "orange",
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "orange",
    alignItems: "center",
    justifyContent: "center",
    padding: 8,
    marginHorizontal: 8,
  },
});
