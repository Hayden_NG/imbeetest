import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Dimensions,
  View,
  Pressable,
  Text,
  Image,
  Linking,
  FlatList,
} from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { useSelector } from "react-redux";
import { RootState } from "../store";

export const QuestionListCard = () => {
  const [questions, setQuestions] = useState<any>([]);
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const presentTag = useSelector((rootState: RootState) => rootState.tagReducer.tag);

  const getQuestionList = async () => {
    try {
      const apiURL = `https://api.stackexchange.com/2.2/questions?page=${page}&pagesize=20&order=desc&sort=activity&tagged=${
        presentTag === "c#" ? "c%23" : presentTag
      }&site=stackoverflow&filter=!Lmz(O)PLdovc0ZsRnA9fdD`;

      await fetch(apiURL)
        .then((res) => res.json())
        .then((resJson) => {
          setQuestions(resJson.items);
          setIsLoading(false);
        });
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getQuestionList();
    setIsLoading(true);
  }, [presentTag, page]);

  const handleLoadmore = () => {
    setPage(page + 1);
    setIsLoading(true);
  };

  const footerSpinner = () => {
    return isLoading ? (
      <View style={styles.spinner}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    ) : null;
  };

  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{ justifyContent: "center", alignItems: "center" }}
      data={questions}
      renderItem={({ item }) => (
        <Pressable style={styles.container} onPress={() => Linking.openURL(item.link)}>
          <Text>{item.title}</Text>
          <View style={styles.content}>
            <View style={styles.questionInfo}>
              <Text>Score</Text>
              <Text style={item.score < 0 ? styles.belowZero : { color: "black" }}>
                {item.score}
              </Text>
            </View>

            <View style={styles.questionInfo}>
              <Text>Answers</Text>
              <View
                style={
                  item.answer_count > 0
                    ? styles.answerMoreThanOneContainer
                    : { backgroundColor: "white" }
                }>
                <Text style={item.answer_count > 0 ? { color: "white" } : { color: "black" }}>
                  {item.answer_count}
                </Text>
              </View>
            </View>

            <View style={styles.questionInfo}>
              <Text>Viewd</Text>
              <Text>{item.view_count}</Text>
            </View>

            <View style={styles.questionInfo}>
              <Image source={{ uri: `${item.owner.profile_image}` }} style={styles.profilePic} />
              <Text>{item.owner.display_name}</Text>
            </View>
          </View>
        </Pressable>
      )}
      keyExtractor={(item, index) => index.toString()}
      onEndReached={handleLoadmore}
      onEndReachedThreshold={0}
      ListFooterComponent={footerSpinner}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get("window").width - 16,
    height: Dimensions.get("window").height / 4,
    backgroundColor: "white",
    marginVertical: 8,
    padding: 16,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "orange",
    justifyContent: "space-evenly",
    elevation: 3,
  },
  content: {
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row",
  },
  questionInfo: {
    flexDirection: "column",
    alignItems: "center",
  },
  profilePic: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  belowZero: {
    color: "red",
  },
  answerMoreThanOneContainer: {
    backgroundColor: "#5fd9a3",
    justifyContent: "center",
    alignItems: "center",
    width: Dimensions.get("window").width / 8,
    height: "auto",
  },
  spinner: {
    marginTop: 8,
    alignItems: "center",
  },
});
