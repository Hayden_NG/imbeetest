import React from "react";
import { Appbar } from "react-native-paper";
import { StyleSheet } from "react-native";

export const Header = () => {
  return (
    <Appbar.Header style={styles.headerContainer}>
      <Appbar.Content title="Trending Topic" color="white" />
    </Appbar.Header>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "orange",
  },
});
