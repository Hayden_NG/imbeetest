import { Tag } from "./reducer";

export const loadTag = (presentTag: Tag) => {
  return {
    type: "@PRESENT_TAG" as "@PRESENT_TAG",
    tag: presentTag,
  };
};

export type TagActions = ReturnType<typeof loadTag>;
