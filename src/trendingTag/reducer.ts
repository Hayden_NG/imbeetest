import { TagActions } from "./actions";

export interface Tag {
  tag: string | null;
}

const initialState: Tag = {
  tag: null,
};

export const tagReducer = (oldState: Tag = initialState, action: TagActions): Tag => {
  switch (action.type) {
    case "@PRESENT_TAG": {
      return action.tag;
    }
    default: {
      return oldState;
    }
  }
};
