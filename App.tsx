import { Header, QuestionListCard, Searching, TabRow } from "components";
import React from "react";
import { StyleSheet, View } from "react-native";

export default function App() {
  return (
    <View style={styles.container}>
      <Header />
      {/* <Searching /> */}
      <TabRow />
      <QuestionListCard />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
