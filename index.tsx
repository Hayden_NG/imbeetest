import React from "react";
import App from "./App";
import { Provider } from "react-redux";
import { registerRootComponent } from "expo";
import { store } from "./src/store";

export const NewRootComponent = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default registerRootComponent(NewRootComponent);
